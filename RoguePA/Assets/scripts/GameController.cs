﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public static GameController Instance;
    public bool areEnemiesMoving;

    private GameObject menu;
    private Text menuText;
    private GameObject levelImage;
    private Text levelText;

    void Start()
    {
        
        menu = GameObject.Find("menu");
        menuText = GameObject.Find("menuText").GetComponent<Text>();
    }

   private void IntitializeGame()
    {

     
        menu = GameObject.Find("menu");
        menu.SetActive(false);
    }

    public void DisableLevelImage()
    {
        levelImage.SetActive(false);
    }


    public void Disablemenu()
    {
        Debug.Log("Disabling menu");
        menu.SetActive(false);
    }

    public void GameOver()
    {
     
        levelText.text = "Game Over";
        levelImage.SetActive(true);
        enabled = false;
    }

}
