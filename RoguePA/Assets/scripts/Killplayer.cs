﻿using UnityEngine;
using System.Collections;

public class Killplayer : MonoBehaviour {

    public Levelmanager Levelmanager;

	// Use this for initialization
	void Start () {
        Levelmanager = FindObjectOfType<Levelmanager>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.name == "player")
        {
            Levelmanager.playerSpawn();
        }
    }
}
