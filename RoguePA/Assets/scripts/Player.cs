﻿

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public bool isGrounded = false;
    public Text healthText;

    private int playerSpeed = 5;
    private int jumpHeight = 1300;
    private Animator animator;
    private int healthPerStone = 10;
    private int playerHealth = 100;
    private int attackPower = 1;

    public Transform lowObject;
    public Transform Respawn;

    //public Transform portal;
    //public Transform spawnpoint;

    void FixedUpdate()
    {
        isGrounded = Physics.Raycast(transform.position, -Vector3.up, 1f);
    }

    // Use this for initialization
     void Start()
    {
        
        animator = GetComponent<Animator>();
        healthText.text ="Health: " + playerHealth;
    }


    // Update is called once per frame
    void Update()
    {
        CheckIfGameOver();

        if (transform.position.y < lowObject.position.y)
        {
            transform.position = Respawn.position;
        }
        


        Vector3 theScale = transform.localScale;
        transform.localScale = theScale;
        animator.SetFloat("playerWalk", 0);
        if (Input.GetKeyDown("right") && theScale.x == -1)
        {
            theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }
        if (Input.GetKeyDown("left") && theScale.x == 1)
        {
            theScale = transform.localScale;
            theScale.x *= 1;
            transform.localScale = theScale;
        }
        if (Input.GetKey("right"))
        {
            transform.position -= Vector3.left * playerSpeed * Time.deltaTime;
            animator.SetFloat("playerWalk", 1);
            
        }

        if (Input.GetKey("left"))
        {

            transform.position -= Vector3.right * playerSpeed * Time.deltaTime;
            animator.SetFloat("playerWalk", 1);
        }

       
        if (Input.GetKeyDown("up") )
        {
            Debug.Log("jump");
            GetComponent<Rigidbody>().AddForce(new Vector3(0, jumpHeight, 0), ForceMode.Force);
            animator.SetTrigger("playerJump");
        }
        if(Input.GetKeyDown("space") )
        {
            animator.SetTrigger("playerAttack");
        }
        
      }  

    private void OnTriggerEnter(Collider objectPlayerCollidedWith)
    {
        if(objectPlayerCollidedWith.tag == "Stone")
        {
            Debug.Log("Collided with Stone");
            playerHealth += healthPerStone;
            healthText.text = "+" + healthPerStone + "Health\n" + "Health: " + playerHealth;
            objectPlayerCollidedWith.gameObject.SetActive(false);
        }
        

    }

    public void TakeDamage(int damageReceived)
    {
        playerHealth -= damageReceived;
        healthText.text = "-" + damageReceived + " Health\n" + "Health: " + playerHealth;
        Debug.Log("Player Health: " + playerHealth);
    }

    private void CheckIfGameOver()
    {
        if(playerHealth >= 0)
        {
            //GameController.Instance.GameOver();
        }
    }

}