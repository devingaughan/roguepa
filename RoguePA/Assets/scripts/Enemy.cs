﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    private float Distance = 0.15f;
    private int lookAtDistance ;
    public int attackDamage;
    private Animator animator;
    
    
   public float min = 1f;
    public float max = 2f;
    // Use this for initialization
    void Start()
    {

        min = transform.position.x;
        max = transform.position.x + 3;

    }

    // Update is called once per frame
    void Update()
    {


        transform.position = new Vector3(Mathf.PingPong(Time.time * 2, max - min) + min, transform.position.y, transform.position.z);

    }
    public void HandleCollision<T>(T component)
    {
        Player player = component as Player;
        player.TakeDamage(attackDamage);
        animator.SetTrigger("enemyAttack");
       
    }

}



  


