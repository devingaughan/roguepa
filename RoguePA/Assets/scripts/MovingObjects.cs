﻿using UnityEngine;
using System.Collections;

public class MovingObjects : MonoBehaviour
{

    public float moveTime = 0.1f;

    private BoxCollider boxCollider;
    private Rigidbody rigidBody;
    private LayerMask collisionLayer;
    private Vector2 endPosition;
    private float inverseMoveTime;
    private RaycastHit hit;

    protected virtual void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
        rigidBody = GetComponent<Rigidbody>();
        collisionLayer = LayerMask.GetMask("Collision");
        inverseMoveTime = 1.0f / moveTime;
    }

    protected virtual void Move<T>(int xDirection, int yDirection)
    {
        RaycastHit2D hit;
        bool canMove = CanObjectMove(xDirection, yDirection, out hit);

        if (canMove)
        {
            return;
        }

        T hitComponent = hit.transform.GetComponent<T>();

        if (hitComponent != null)
        {
            HandleCollision(hitComponent);
        }

    }



    protected bool CanObjectMove(int xDirection, int yDirection, out RaycastHit2D hit)
    {
        Vector2 startPosition = rigidBody.position;
        endPosition = startPosition + new Vector2(xDirection, yDirection);

        boxCollider.enabled = false;
        hit = Physics2D.Linecast(startPosition, endPosition, collisionLayer);
        boxCollider.enabled = true;

        if (hit.transform == null)
        {
         
            return true;
        }





        return false;
    }

    




    


    /* {

         Vector2 updatedPosition = Vector2.MoveTowards(rigidBody.position, endPosition, inverseMoveTime * Time.deltaTime);
         rigidBody.MovePosition(updatedPosition);
         yield return null;
     } 

 }
 */

    public void HandleCollision<T>(T component)
    {
      
    }

}




